RSpec.describe Status::Page do
  it "has a version number" do
    expect(Status::Page::VERSION).not_to be nil
  end

  it "it should output to a file on pull" do
    msg = Status::Page::CLI.new.pull
    expect(msg).not_to be nil
  end

  it "live should print live status on screen " do

    # this is an infinite loop , so we will test it once then kill it
    thread = Thread.new do
      msg = Status::Page::CLI.new.live
      expect(msg).not_to be nil
      thread.kill
    end

  end

  it "backup should create a file" do
    Status::Page::CLI.new.backup('/tmp/test123')
    # expect(msg).not_to be nil
    expect(File).to exist("/tmp/test123")
  end

  it "restore should create the default file" do
    Status::Page::CLI.new.restore("/tmp/test123")
    expect(File).to exist("sites_status")
  end


end
