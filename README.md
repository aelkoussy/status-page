# Status::Page

a simple Ruby cli gem that query the status of some websites (bitbucket, cloudflare, rubygems, github)

## Installation

1- Download the gem

2- Extract it to a folder

3- Open a terminal & cd into that folder

4- Run `bundle install`

5- Run `chmod +x exe/status-page`

6- Run `bundle exec exe/status-page` with any of the arguments available (without args it will show you the help for available commands)


Note:
for filepath args, you have write explicit path including the filename

## Usage

You can use the following options in this gem:

"live": "constantly query the URLs and output the status periodically on the console and save it to the data store."

"history": "display all the data which was gathered by the tool"

"backup [filepath]": "takes a path variable and creates a backup of historic and currently saved data."

"restore [filepath]": "takes a path variable which is a backup created by the application and restores that data"

Note:
for filepath args, you have write explicit path including the filename

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Status::Page project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/aelkoussy/status-page/blob/master/CODE_OF_CONDUCT.md).
