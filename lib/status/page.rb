require "status/page/version"
require 'thor'
require "nokogiri"
require 'open-uri'
require 'date'

module Status
  module Page
    class CLI < Thor
      desc "pull", "query website status"
      def pull

        # status URLs that we will use
        urls = ["https://status.github.com/messages", "https://status.bitbucket.org/","https://www.cloudflarestatus.com/",
          "https://status.rubygems.org/"]

        # looping on the Status URLs, fetching status & displaying it for each URL
        urls.each {|x| 
          website_status = "#{x.ljust(40)}: #{check_status(x).ljust(30)} at #{Time.now.strftime("%d/%m/%Y %H:%M")}"
          puts website_status
          File.open("sites_status", 'a') { |file| file.write(website_status + "\n") }
        }
        File.open("sites_status", 'a') { |file| file.write("========" + "\n") }
      end


      desc "live", "constantly query the URLs and output the status periodically on the console and save it to the data store."
      def live
        while true do
          pull
          puts "========"
          sleep 60 #seconds
        end
      end

      desc "history", "display all the data which was gathered by the tool"
      def history
        txt = open("sites_status")
        puts txt.read
      end

      desc "backup [filepath]", "takes a path variable, and creates a backup of historic and currently saved data (filepath includes filename)"
      def backup(filepath)
        txt = open("sites_status")
        input_text = txt.read
        File.open(filepath, 'w') { |file| file.write(input_text) }
      end

      desc "restore [filepath]", "takes a path variable which is a backup created by the application and restores that data (filepath includes filename)"
      def restore(filepath)
        txt = open(filepath)
        input_text = txt.read
        File.open("sites_status", 'w') { |file| file.write(input_text) }
      end

      # no_commands is used to not show this as a command in our CLI gem

      no_commands { 

      # This is a helper method that will fetch the HTML & parse it using Nokogiri
        def check_status(url)
          data = Nokogiri::HTML(open(url))
          # puts data
          # Only github uses a different way to show status
            if url == "https://status.github.com/messages"
              status = data.css("span.title")[0].text
              # All other websites are using a similar pattern for the main status
            else
              status = data.css("span.status.font-large").text
            end

          # remove trailing & preceding whitespace to keep it looking uniformally
          return status.strip
        end
      }
    end
  end
end
